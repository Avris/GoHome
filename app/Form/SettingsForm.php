<?php
namespace App\Form;

use App\Model\Settings;
use Avris\Micrus\Forms\Form;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Widget\ChoiceHelper;

class SettingsForm extends Form
{
    public function configure()
    {
        $this
            ->add('mode', 'ButtonChoice', [
                'label' => '',
                'choices' => Settings::$modes,
                'multiple' => false,
            ], new Assert\NotBlank())
            ->add('start', 'Time',  [
                'label' => 'What time did you start working today?',
                'attr' => ['data-showonly' => Settings::MODE_CLOCK],
            ], new Assert\NotBlank())
            ->add('hours', 'NumberAddon',  [
                'label' => 'How long should you work today?',
                'after' => 'hours',
            ], [
                new Assert\NotBlank(),
                new Assert\Min(0),
                new Assert\Max(16),
            ])
            ->add('togglKey', 'Text',  [
                'label' => 'Toggl API token',
                'attr' => ['data-showonly' => Settings::MODE_TOGGL],
            ])
            ->add('break', 'NumberAddon',  [
                'label' => 'Time wasted on eating lunch',
                'after' => 'min',
                'attr' => ['data-showonly' => Settings::MODE_CLOCK],
            ], [
                new Assert\NotBlank(),
                new Assert\Min(0),
                new Assert\Max(600),
            ])
            ->add('timezone', 'Choice', [
                'label' => 'Timezone',
                'choices' => ChoiceHelper::$timezone,
                'attr' => ['data-showonly' => Settings::MODE_CLOCK],
            ])
            ->add('sound', 'Checkbox', [
                'label' => '',
                'sublabel' => '<span class="fa fa-volume-down fa-fw"></span> Play sounds',
            ])
            ->add('favicon', 'Checkbox', [
                'label' => '',
                'sublabel' => '<span class="fa fa-clock-o fa-fw"></span> Show countdown in the favicon',
            ])
        ;
    }
}
