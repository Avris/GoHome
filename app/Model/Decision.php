<?php
namespace App\Model;

use AJT\Toggl\TogglClient;
use ICanBoogie\DateTime;

class Decision
{
    public $secondsLeft;

    public function __construct(Settings $settings)
    {
        $secondsDone = call_user_func([$this, $settings->mode . 'CountSeconds'], $settings);

        $this->secondsLeft = ($settings->hours * 60 * 60) - $secondsDone;
    }

    public function canGoHome()
    {
        return $this->secondsLeft <= 0;
    }

    public function getTimeLeft()
    {
        if ($this->canGoHome()) {
            return null;
        }

        return sprintf(
            '%s:%s:%s',
            str_pad(floor($this->secondsLeft / 60 / 60), 2, '0', STR_PAD_LEFT),
            str_pad(floor($this->secondsLeft / 60) % 60, 2, '0', STR_PAD_LEFT),
            str_pad($this->secondsLeft % 60,             2, '0', STR_PAD_LEFT)
        );
    }

    protected function clockCountSeconds(Settings $settings)
    {
        date_default_timezone_set($settings->timezone);

        return $this->diff(DateTime::now(), DateTime::from('today ' . $settings->start))
            - $settings->getBreak() * 60;
    }

    protected function togglCountSeconds(Settings $settings)
    {
        $togglClient = TogglClient::factory(['api_key' => $settings->togglKey]);

        $user = $togglClient->getCurrentUser();

        date_default_timezone_set($user['timezone']);

        $todayEntries = $togglClient->getTimeEntries([
            'start_date' => DateTime::from('00:00:00')->format('c'),
            'end_date' => DateTime::from('23:59:59')->format('c'),
        ]);

        $seconds = 0;
        foreach ($todayEntries as $entry) {
            $seconds += isset($entry['stop'])
                ? $entry['duration']
                : $this->diff(DateTime::now(), DateTime::from($entry['start'])->local);
        }

        return $seconds;
    }

    protected function diff(DateTime $date1, DateTime $date2)
    {
        $diff = $date1->diff($date2);

        return $diff->invert ? ($diff->s + $diff->i * 60 + $diff->h * 60 * 60) : 0;
    }
}
