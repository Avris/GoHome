<?php
namespace App\Model;

class Settings
{
    const MODE_CLOCK = 'clock';
    const MODE_TOGGL = 'toggl';

    public static $modes = [
        self::MODE_CLOCK => 'Manual',
        self::MODE_TOGGL =>  'With Toggl',
    ];

    public $mode = self::MODE_CLOCK;

    public $confirmed = false;

    public $start = '09:00';

    public $hours = 8;

    public $togglKey;

    public $sound = false;

    public $favicon = true;

    private $break = 0;

    private $breakSetAt;

    public $timezone = 'UTC';

    public function getBreak()
    {
        if ($this->mode === self::MODE_CLOCK) {
            return $this->breakSetAt == date('Y-m-d') ? $this->break : 0;
        }

        return 0;
    }

    public function setBreak($break)
    {
        $this->break = $break;
        $this->breakSetAt = date('Y-m-d');
        return $this;
    }
}
