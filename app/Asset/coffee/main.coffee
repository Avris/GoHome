pad = (n) -> if n < 10 then '0'+n else n

class GoHomeFavico
  @lastBadge = false
  @options = {}
  constructor: (options) ->
    @favico = new Favico(options)
  update: (hoursLeft, minutesLeft) ->
    [badge, options] = if hoursLeft then ["#{pad hoursLeft}:", {bgColor: '#d00'}] else [":#{if minutesLeft then minutesLeft else ')'}", {bgColor: '#0a4'}]
    if badge != @lastBadge
      @favico.badge badge, options
      @lastBadge = badge

$fill = $('.fill')
resize = ->
  $fill.css 'height', ''
  $fill.each -> $(this).css 'height', Math.max($(this).height(), window.innerHeight, $(document).height())
resize()
$(window).resize(resize)

$('#settings-btn').click ->
  $('.settings').toggleClass 'hidden-opac'
  resize()

if @decisionUrl?
  $timer = $('.timer')
  @favicon = new GoHomeFavico({animation: 'fade'})
  originalTitle = document.title

  doCountdown = =>
    if @secondsLeft <= 0
      @secondsLeft = 0
      clearInterval countdown
      window.location.reload()
    hoursLeft = Math.floor(@secondsLeft / 60 / 60)
    minutesLeft = Math.floor(@secondsLeft / 60) % 60
    secondsLeft = @secondsLeft % 60
    timer = "#{pad hoursLeft}:#{pad minutesLeft}:#{pad secondsLeft}"
    $timer.html timer
    document.title = "[#{timer}] #{originalTitle}"
    favicon.update hoursLeft, minutesLeft if window.useFavico
    @secondsLeft--

  countdown = setInterval doCountdown, 1000
  doCountdown()

  refresh = setInterval =>
    $.post @decisionUrl, (data) => @secondsLeft = parseInt(data)
  , 1000 * 60

$tz = $('#Settings_timezone')
if $tz.val() == 'UTC'
  $tz.val(jstz.determine().name())

$mode = $('[name=Settings\\[mode\\]]')
updateMode = ->
  mode = $mode.filter(':checked').val()
  widgets = $('.form-control[data-showonly]')
  widgets.removeAttr('required').parents('.form-group').addClass('hidden')
  widgets.filter("[data-showonly=#{mode}]").attr('required', 'required').parents('.form-group').removeClass('hidden')
  resize()
$mode.change updateMode
updateMode()
