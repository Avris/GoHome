<?php
namespace App\Controller;

use App\Form\SettingsForm;
use App\Model\Decision;
use App\Model\Settings;
use Avris\Micrus\Controller\Controller;
use Avris\Micrus\Controller\Http\Response;
use Guzzle\Http\Exception\ClientErrorResponseException;

class HomeController extends Controller
{
    public function homeAction()
    {
        if ($this->getQuery()->has('clear')) {
            $this->setSettings(new Settings());
            return $this->redirectToRoute('home');
        }

        $form = new SettingsForm($settings = $this->getSettings());

        $form->bind($this->getData());
        if ($form->isValid()) {
            $settings->confirmed = true;
            $this->setSettings($settings);
        }

        try {
            $decision = $settings->confirmed ? new Decision($settings) : null;
        } catch (ClientErrorResponseException $e) {
            $this->addFlash('error', 'Specified Toggl API Key is not valid...');
            $settings->confirmed = false;
            $this->setSettings($settings);
            return $this->redirectToRoute('home');
        }

        if ($this->getRequest()->isAjax()) {
            return new Response($decision->secondsLeft);
        }

        return $this->render([
            '_view' => 'index',
            'form' => $form,
            'settings' => $settings,
            'decision' => $decision,
        ]);
    }

    protected function getSettings()
    {
        return $this->getSession('settings') ?: unserialize($this->getCookies('settings')) ?: new Settings();
    }

    protected function setSettings(Settings $settings)
    {
        $this->getSession()->set('settings', $settings);
        $this->getCookies()->set('settings', serialize($settings), time() + 60*60*24*30);
        return $settings;
    }

}
